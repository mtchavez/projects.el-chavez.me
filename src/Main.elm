module Main exposing (..)

import Browser exposing (Document)
import Html exposing (Html, a, button, div, h2, h3, i, li, p, s, text, ul)
import Html.Attributes exposing (attribute, class, href, id, rel, title)
import Html.Events exposing (onClick)



---- MODEL ----


type alias Model =
    { rubyProjects : List Project
    , goProjects : List Project
    , exProjects : List Project
    , erlProjects : List Project
    , pythonProjects : List Project
    , ansibleProjects : List Project
    , menu : List MenuItem
    , menuExpanded : Bool
    }


type alias Project =
    { title : String
    , href : String
    , description : Html Msg
    , langIcon : Maybe LangIcon
    }


type alias LangIcon =
    { title : String
    , iconClass : String
    }


type alias MenuItem =
    { title : String
    , href : String
    }


menuItems : List MenuItem
menuItems =
    [ { title = "el-chavez.me", href = "https://el-chavez.me" }
    , { title = "Blog", href = "https://blog.el-chavez.me" }
    , { title = "Résumé", href = "https://el-chavez.me/about/resume.pdf" }
    ]


initialRubyProjects : List Project
initialRubyProjects =
    [ { title = "CircleCI Gem"
      , href = "https://github.com/mtchavez/circleci"
      , description =
            p
                []
                [ text "Implements the "
                , a [ href "https://circleci.com/docs/api", rel "noopener" ]
                    [ text "CircleCi REST API" ]
                , text ". Fully tested, documented, and maintains a high Code Climate maintainability rating."
                ]
      , langIcon = Just { iconClass = "devicon-ruby-plain colored", title = "Ruby" }
      }
    , { title = "Iterable Gem"
      , href = "https://gitlab.com/mtchavez/iterable"
      , description =
            p []
                [ text "A "
                , a [ href "https://rubygems.org/gems/iterable-api-client", rel "noopener" ]
                    [ text "gem" ]
                , text " for "
                , a [ href "https://iterable.com/", rel "noopener" ]
                    [ text "Iterable" ]
                , text " that implements their "
                , a [ href "https://api.iterable.com/api/docs", rel "noopener" ]
                    [ text "REST API" ]
                , text ". Fully tested and documented"
                ]
      , langIcon = Just { iconClass = "devicon-ruby-plain colored", title = "Ruby" }
      }
    , { title = "AL PAPI Gem"
      , href = "https://github.com/mtchavez/al_papi"
      , description =
            p []
                [ text "Gem built for "
                , a [ href "https://api.authoritylabs.com/", rel "noopener" ]
                    [ text "AuthorityLabs Partner API" ]
                , text ". Used internally on AuthorityLabs projects and open sourced it for clients to quickly integrate the API."
                ]
      , langIcon = Just { iconClass = "devicon-ruby-plain colored", title = "Ruby" }
      }
    ]


initialGoProjects : List Project
initialGoProjects =
    [ { title = "Cuckoo Filter"
      , href = "https://github.com/mtchavez/cuckoo"
      , description =
            p [] [ text "Cuckoo Filter sketching algorithm for approximations of set membership." ]
      , langIcon = Just { iconClass = "devicon-go-plain colored", title = "Golang" }
      }
    , { title = "Hyper-Log-Log"
      , href = "https://github.com/mtchavez/go-hll"
      , description =
            p [] [ text "Hyper Log Log sketching algorithm written in Golang." ]
      , langIcon = Just { iconClass = "devicon-go-plain colored", title = "Golang" }
      }
    , { title = "Countmin"
      , href = "https://github.com/mtchavez/countmin"
      , description =
            p [] [ text "Count min sketching algorithm to get the minimum number of occurrences of a key in a stream." ]
      , langIcon = Just { iconClass = "devicon-go-plain colored", title = "Golang" }
      }
    , { title = "Skiplist"
      , href = "https://github.com/mtchavez/skiplist"
      , description =
            p [] [ text "Skip List implementation in golang that allows for duplicate keys where the values can be the same or different in the list." ]
      , langIcon = Just { iconClass = "devicon-go-plain colored", title = "Golang" }
      }
    , { title = "LFU Cache"
      , href = "https://github.com/mtchavez/lfu"
      , description =
            p [] [ text "LFU cache in golang offering O(1) Get and Insert." ]
      , langIcon = Just { iconClass = "devicon-go-plain colored", title = "Golang" }
      }
    , { title = "Jump Consistent Hash"
      , href = "https://github.com/mtchavez/jumpconshash"
      , description =
            p [] [ text "Jump consistent hash package. Built from Google whitepaper where it was introduced." ]
      , langIcon = Just { iconClass = "devicon-go-plain colored", title = "Golang" }
      }
    , { title = "Go Statsite"
      , href = "https://github.com/mtchavez/go-statsite"
      , description =
            p []
                [ text "Golang package to send stats to a "
                , a [ href "https://github.com/armon/statsite", rel "noopener" ]
                    [ text "statsite" ]
                , text " instance from your code. Statsite is a StatsD compliant server written in C."
                ]
      , langIcon = Just { iconClass = "devicon-go-plain colored", title = "Golang" }
      }
    ]


initialExProjects : List Project
initialExProjects =
    [ { title = "Üeberauth Gitlab"
      , href = "https://github.com/mtchavez/ueberauth_gitlab"
      , description =
            p []
                [ text "An Üeberauth OAuth2 strategy for "
                , a [ href "https://gitlab.com/", rel "noopener" ]
                    [ text "Gitlab" ]
                , text ". Integrates with "
                , a [ href "https://github.com/ueberauth/ueberauth", rel "noopener" ]
                    [ text "Üeberauth" ]
                , text " to implement an authentication provider of Gitlab. Docs can be found "
                , a [ href "https://hexdocs.pm/ueberauth_gitlab_strategy", rel "noopener" ]
                    [ text "here." ]
                ]
      , langIcon = Nothing
      }
    , { title = "CC Validation"
      , href = "https://github.com/mtchavez/ex_cc_validation"
      , description =
            p []
                [ text "A "
                , a [ href "https://hex.pm/packages/cc_validation", rel "noopener" ]
                    [ text "Hex package" ]
                , text " to validate credit card format using the "
                , a [ href "https://wikipedia.org/wiki/Luhn_algorithm", rel "noopener" ]
                    [ text "Luhn Algorithm" ]
                , text ". Test cards are also allowed to be checked for. See "
                , a [ href "https://hexdocs.pm/cc_validation/CcValidation.html", rel "noopener" ]
                    [ text "documentation" ]
                , text " for more details."
                ]
      , langIcon = Nothing
      }
    ]


initialErlProjects : List Project
initialErlProjects =
    [ { title = "BK Tree"
      , href = "https://github.com/mtchavez/bk-tree-erl"
      , description =
            p []
                [ text "A "
                , a [ href "https://en.wikipedia.org/wiki/BK-tree", rel "noopener" ]
                    [ text "BK Tree" ]
                , text " in Erlang"
                ]
      , langIcon = Just { iconClass = "devicon-erlang-plain colored", title = "Erlang" }
      }
    , { title = "Binary Search Tree"
      , href = "https://github.com/mtchavez/binary-search-erl"
      , description =
            p []
                [ text "A "
                , a [ href "https://en.wikipedia.org/wiki/Binary_search_tree", rel "noopener" ]
                    [ text "Binary Search Tree" ]
                , text "in Erlang."
                ]
      , langIcon = Just { iconClass = "devicon-erlang-plain colored", title = "Erlang" }
      }
    , { title = "Trie"
      , href = "https://github.com/mtchavez/trie-erl"
      , description =
            p []
                [ text "The "
                , a [ href "https://en.wikipedia.org/wiki/Trie", rel "noopener" ]
                    [ text "Trie data structure" ]
                , text "in Erlang."
                ]
      , langIcon = Just { iconClass = "devicon-erlang-plain colored", title = "Erlang" }
      }
    ]


initialPythonProjects : List Project
initialPythonProjects =
    [ { title = "Package Boilerplate"
      , href = "https://github.com/mtchavez/python-package-boilerplate"
      , description =
            p []
                [ text "A python "
                , a [ href "https://pypi.org/", rel "noopener" ]
                    [ text "PyPI Package" ]
                , text " boilerplate to make setting up a new package easier."
                ]
      , langIcon = Just { iconClass = "devicon-python-plain colored", title = "Python" }
      }
    ]


initialAnsibleProjects : List Project
initialAnsibleProjects =
    [ { title = "InfluxDB Role"
      , href = "https://github.com/mtchavez/ansible-influxdb"
      , description =
            p []
                [ a [ href "https://galaxy.ansible.com", rel "noopener" ]
                    [ text "Ansible Galaxy" ]
                , text " role for setting up "
                , a [ href "https://github.com/influxdata/influxdb", rel "noopener" ]
                    [ text "InfluxDB." ]
                ]
      , langIcon = Nothing
      }
    , { title = "Mac Ansible"
      , href = "https://github.com/mtchavez/mac-ansible"
      , description =
            p [] [ text "Ansible setup to provision a Mac OS X machine for development and personal use." ]
      , langIcon = Nothing
      }
    , { title = "Consul Template"
      , href = "https://github.com/mtchavez/ansible-consul-template"
      , description =
            p [] [ text "Ansible role to setup/install consul template" ]
      , langIcon = Nothing
      }
    , { title = "Envconsul"
      , href = "https://github.com/mtchavez/ansible-envconsul"
      , description =
            p [] [ text "Ansible role to setup/install envconsul" ]
      , langIcon = Nothing
      }
    , { title = "Statsite"
      , href = "https://github.com/mtchavez/ansible-statsite"
      , description =
            p [] [ text "Ansible role to setup/install statsite, a statsd implementation in C" ]
      , langIcon = Nothing
      }
    ]


initialModel : Model
initialModel =
    { rubyProjects = initialRubyProjects
    , goProjects = initialGoProjects
    , exProjects = initialExProjects
    , erlProjects = initialErlProjects
    , pythonProjects = initialPythonProjects
    , ansibleProjects = initialAnsibleProjects
    , menu = menuItems
    , menuExpanded = False
    }


init : ( Model, Cmd Msg )
init =
    ( initialModel, Cmd.none )



---- MESSAGES ----


type Msg
    = Expand
    | Collapse



---- UPDATE ----


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Expand ->
            ( { model | menuExpanded = True }, Cmd.none )

        Collapse ->
            ( { model | menuExpanded = False }, Cmd.none )



---- VIEW ----


menuItemView : MenuItem -> Html Msg
menuItemView item =
    li [ class "pure-menu-item" ]
        [ a [ href item.href, class "pure-menu-link", rel "noopener" ]
            [ text item.title ]
        ]


menuToggle : Model -> Html Msg
menuToggle model =
    button
        [ id "toggle"
        , title "menu-toggle"
        , attribute "aria-label" "Menu Toggle"
        , class
            (if model.menuExpanded then
                "custom-toggle x"

             else
                "custom-toggle"
            )
        , onClick
            (if model.menuExpanded then
                Collapse

             else
                Expand
            )
        ]
        [ s [ class "bar" ]
            []
        , s [ class "bar" ]
            []
        ]


menuWithItems : Model -> Html Msg
menuWithItems model =
    div
        [ class
            (if model.menuExpanded then
                "pure-menu custom-can-transform"

             else
                "pure-menu pure-menu-horizontal custom-can-transform"
            )
        ]
        [ ul [ class "pure-menu-list" ]
            (List.map menuItemView menuItems)
        ]


menu : Model -> Html Msg
menu model =
    div
        [ id "menu"
        , class
            (if model.menuExpanded then
                "custom-wrapper open pure-g"

             else
                "custom-wrapper pure-g"
            )
        ]
        [ div [ class "pure-u-1 pure-u-md-1-3" ]
            [ div [ class "pure-menu" ]
                [ a [ href "/", class "pure-menu-heading custom-menu-brand" ]
                    [ text "Chavez Projects" ]
                , menuToggle model
                ]
            ]
        , div [ class "pure-u-1 pure-u-md-1-3" ]
            [ menuWithItems model
            ]
        ]


projectsSectionView : String -> List Project -> Html Msg
projectsSectionView title projects =
    div [ class "pure-u-1" ]
        [ h2 [ class "section-title" ]
            [ text title ]
        , div [ class "projects-section" ]
            (List.map projectView projects)
        ]


projectView : Project -> Html Msg
projectView project =
    let
        iconClass =
            case project.langIcon of
                Nothing ->
                    ""

                Just langIcon ->
                    langIcon.iconClass
    in
    div [ class "pure-u-1 pure-u-md-1-2 pure-u-lg-1-4" ]
        [ div [ class "l-box" ]
            [ h3 [ class "box-title" ]
                [ a [ href project.href, rel "noopener", title project.title ]
                    [ text project.title ]
                , i [ class (String.append "lang-icon " iconClass) ] []
                ]
            , project.description
            ]
        ]


view : Model -> Html Msg
view model =
    div [ class "content" ]
        [ menu model
        , div [ class "pure-g" ]
            [ projectsSectionView "Ruby" model.rubyProjects
            , projectsSectionView "Golang" model.goProjects
            , projectsSectionView "Elixir" model.exProjects
            , projectsSectionView "Erlang" model.erlProjects
            , projectsSectionView "Python" model.pythonProjects
            , projectsSectionView "Ansible" model.ansibleProjects
            ]
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> init
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }
